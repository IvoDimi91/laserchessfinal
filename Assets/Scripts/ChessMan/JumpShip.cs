﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpShip : ChessMan {

    public float rotationAngle;

    public JumpShip()
    {
        playerCode = 0;
        chessManCode = 2;
        attackPower = 2;
        hitPoints = 2;
        maximumHP = 2;
        canAttackSimulteniously = true;
    }

    public override void SpawnChessMan(GameObject chessManPrefab)
    {
        base.SpawnChessMan(chessManPrefab);
        transform.rotation = Quaternion.AngleAxis(rotationAngle, Vector3.up);
    }

    public override bool[,] PossibleMoves()
    {
        bool[,] pM = new bool[GameManager.GetInstance().boardSize, GameManager.GetInstance().boardSize];
        int x, y;

        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);

        //UpLeft
        KnightMoves(x - 1, y + 2, ref pM);

        //UpRight
        KnightMoves(x + 1, y + 2, ref pM);

        //DownLeft
        KnightMoves(x - 1, y - 2, ref pM);

        //DownRight
        KnightMoves(x + 1, y - 2, ref pM);

        //RightUp
        KnightMoves(x + 2, y + 1, ref pM);

        //RightDown
        KnightMoves(x + 2, y - 1, ref pM);

        //LeftUp
        KnightMoves(x - 2, y + 1, ref pM);

        //LeftDown
        KnightMoves(x - 2, y - 1, ref pM);
        return pM;
    }

    public override bool[,] PossibleAttacks(Vector2 gridPosition)
    {
        bool[,] pA = new bool[GameManager.GetInstance().boardSize, GameManager.GetInstance().boardSize];
        int x, y;


        ChessMan c = null;

        //left
        x = (int)Mathf.Floor(gridPosition.x) - 1;
        y = (int)Mathf.Floor(gridPosition.y);

        if (x >= 0)
        {
            c = GameManager.GetInstance().chessMans[x, y];
            if (c != null && c.playerCode != this.playerCode) {
                pA[x, y] = true;
            }
        }

        //right
        x = (int)Mathf.Floor(gridPosition.x) + 1;
        y = (int)Mathf.Floor(gridPosition.y);

        if (x <= 7)
        {
            c = GameManager.GetInstance().chessMans[x, y];
            if (c != null && c.playerCode != this.playerCode) {
                pA[x, y] = true;
            }
        }
        //up
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y) + 1;

        if (y <= 7)
        {
            c = GameManager.GetInstance().chessMans[x, y];
            if (c != null && c.playerCode != this.playerCode) {
                pA[x, y] = true;
            }
        }

        //down
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y) - 1;

        if (y >= 0)
        {
            c = GameManager.GetInstance().chessMans[x, y];
            if (c != null && c.playerCode != this.playerCode) {
                pA[x, y] = true;
            }
        }
        return pA;
    }

    private void KnightMoves(int x, int y, ref bool[,] r)
    {
        ChessMan c;
        if (x >= 0 && x < 8 && y >= 0 && y < 8)
        {
            c = GameManager.GetInstance().chessMans[x, y];
            if (c == null)
            {
                r[x, y] = true;
            }
        }
    }

    public override void MoveChessMan()
    {
        base.MoveChessMan();
    }

    public override void AttackOpponentsChessMan(List<ChessMan> opponentChessMans)
    {
        base.AttackOpponentsChessMan(opponentChessMans);
    }
}
