﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grunt : ChessMan {

    public Grunt()
    {
        playerCode = 0;
        chessManCode = 1;
        attackPower = 1;
        hitPoints = 2;
        maximumHP = 2;
        canAttackSimulteniously = false;
}

    public override void SpawnChessMan(GameObject chessManPrefab)
    {
        base.SpawnChessMan(chessManPrefab);
    }

    public override bool[,] PossibleMoves()
    {
      
        bool[,] pM = new bool[GameManager.GetInstance().boardSize, GameManager.GetInstance().boardSize];
        int x, y;

        //left
        x = (int)Mathf.Floor(gridPosition.x) - 1;
        y = (int)Mathf.Floor(gridPosition.y);
        if (x >= 0 && GameManager.GetInstance().chessMans[x, y] == null)
        {
            pM[x, y] = true;
        }

        //right
        x = (int)Mathf.Floor(gridPosition.x) + 1;
        y = (int)Mathf.Floor(gridPosition.y);
        if (x <= 7 && GameManager.GetInstance().chessMans[x, y] == null)
        {
            pM[x, y] = true;
        }
        //up
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y) + 1;

        if (y <= 7 && GameManager.GetInstance().chessMans[x, y] == null)
        {
            pM[x, y] = true;
        }

        //down
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y) - 1;

        if (y >= 0 && GameManager.GetInstance().chessMans[x, y] == null)
        {
            pM[x, y] = true;
        }

        return pM;
    }

    public override void MoveChessMan()
    {
        base.MoveChessMan();
    }

    public override bool[,] PossibleAttacks(Vector2 gridPosition)
    {
        bool[,] pA = new bool[GameManager.GetInstance().boardSize, GameManager.GetInstance().boardSize];
        int x, y;

        ChessMan c;

        //TopLeft
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);

        while (true)
        {

            x--;
            y++;

            if (x < 0 || y >= 8)
            {
                break;
            }

            c = GameManager.GetInstance().chessMans[x, y];
            if(c != null)
            {
                if (this.playerCode != c.playerCode)
                {
                    pA[x, y] = true;
                }
                break;
            }
        }

        //TopRight
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);

        while (true)
        {

            x++;
            y++;

            if (x >= 8 || y >= 8)
            {
                break;
            }

            c = GameManager.GetInstance().chessMans[x, y];
            if (c != null)
            {
                if (this.playerCode != c.playerCode)
                {
                    pA[x, y] = true;
                }
                break;
            }
        }

        //BottomLeft
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);

        while (true)
        {

            x--;
            y--;

            if (x < 0 || y < 0)
            {
                break;
            }

            c = GameManager.GetInstance().chessMans[x, y];
            if (c != null)
            {
                if (this.playerCode != c.playerCode)
                {
                    pA[x, y] = true;
                }
                break;
            }
        }

        //BottomRight
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);

        while (true)
        {

            x++;
            y--;

            if (x >= 8 || y < 0)
            {
                break;
            }

            c = GameManager.GetInstance().chessMans[x, y];
            if (c != null)
            {
                if (this.playerCode != c.playerCode)
                {
                    pA[x, y] = true;
                }
                break;
            }
        }

        return pA;
    }

    public override void AttackOpponentsChessMan(List<ChessMan> opponentChessMans)
    {
        base.AttackOpponentsChessMan(opponentChessMans);

    }
}
