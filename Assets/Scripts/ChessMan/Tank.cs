﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank : ChessMan {

    public Tank()
    {
        playerCode = 0;
        chessManCode = 3;
        attackPower = 2;
        hitPoints = 4;
        maximumHP = 4;
        canAttackSimulteniously = false;
    }

    public override void SpawnChessMan(GameObject chessManPrefab)
    {
        base.SpawnChessMan(chessManPrefab);
    }

    public override bool[,] PossibleMoves()
    {
        bool[,] pM = new bool[GameManager.GetInstance().boardSize, GameManager.GetInstance().boardSize];
        int x, y;

        int maxPermitedSpaces = 3;

        ChessMan c;

        // Right
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        while (maxPermitedSpaces > 0)
        {
            x++;
            if (x >= 8)
            {
                break;
            }
            c = GameManager.GetInstance().chessMans[x, y];
            if (c == null)
            {
                pM[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        //Left
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        maxPermitedSpaces = 3;
        while (maxPermitedSpaces > 0)
        {
            x--;
            if (x < 0)
            {
                break;
            }
            c = GameManager.GetInstance().chessMans[x, y];
            if (c == null)
            {
                pM[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        //Up
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        maxPermitedSpaces = 3;
        while (maxPermitedSpaces > 0)
        {
            y++;
            if (y >= 8)
            {
                break;
            }
            c = GameManager.GetInstance().chessMans[x, y];
            if (c == null)
            {
                pM[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        //Down
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        maxPermitedSpaces = 3;
        while (maxPermitedSpaces > 0)
        {
            y--;
            if (y < 0)
            {
                break;
            }
            c = GameManager.GetInstance().chessMans[x, y];
            if (c == null)
            {
                pM[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        //TopLeft
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        maxPermitedSpaces = 3;
        while (maxPermitedSpaces > 0)
        {

            x--;
            y++;

            if (x < 0 || y >= 8)
            {
                break;
            }
            c = GameManager.GetInstance().chessMans[x, y];
            if (c == null)
            {
                pM[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        //TopRight
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        maxPermitedSpaces = 3;
        while (maxPermitedSpaces > 0)
        {

            x++;
            y++;

            if (x >= 8 || y >= 8)
            {
                break;
            }

            c = GameManager.GetInstance().chessMans[x, y];
            if (c == null)
            {
                pM[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        //DownLeft
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        maxPermitedSpaces = 3;
        while (maxPermitedSpaces > 0)
        {

            x--;
            y--;

            if (x < 0 || y < 0)
            {
                break;
            }

            c = GameManager.GetInstance().chessMans[x, y];
            if (c == null)
            {
                pM[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        //DownRight
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);
        maxPermitedSpaces = 3;
        while (maxPermitedSpaces > 0)
        {

            x++;
            y--;

            if (x >= 8 || y < 0)
            {
                break;
            }

            c = GameManager.GetInstance().chessMans[x, y];
            if (c == null)
            {
                pM[x, y] = true;
            }
            else
            {
                break;
            }
            maxPermitedSpaces--;
        }

        return pM; 
    }

    public override bool[,] PossibleAttacks(Vector2 gridPosition)
    {
        bool[,] pA = new bool[GameManager.GetInstance().boardSize, GameManager.GetInstance().boardSize];
        int x, y;

        ChessMan c;

        //Left
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);

        while (true)
        {
            x--;

            if (x < 0)
            {
                break;
            }

            c = GameManager.GetInstance().chessMans[x, y];
            if (c != null)
            {
                if (this.playerCode != c.playerCode)
                {
                    pA[x, y] = true;
                }
                break;
            }
        }

        //Right
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);

        while (true)
        {
            x++;
           
            if (x >= 8)
            {
                break;
            }

            c = GameManager.GetInstance().chessMans[x, y];
            if (c != null)
            {
                if (this.playerCode != c.playerCode)
                {
                    pA[x, y] = true;
                }
                break;
            }
        }

        //Up
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);

        while (true)
        {
            y++;

            if (y >= 8)
            {
                break;
            }

            c = GameManager.GetInstance().chessMans[x, y];
            if (c != null)
            {
                if (this.playerCode != c.playerCode)
                {
                    pA[x, y] = true;
                }
                break;
            }
        }

        //Down
        x = (int)Mathf.Floor(gridPosition.x);
        y = (int)Mathf.Floor(gridPosition.y);

        while (true)
        {
            y--;

            if (y < 0)
            {
                break;
            }

            c = GameManager.GetInstance().chessMans[x, y];
            if (c != null)
            {
                if (this.playerCode != c.playerCode)
                {
                    pA[x, y] = true;
                }
                break;
            }
        }

        return pA;
    }

    public override void MoveChessMan()
    {
        base.MoveChessMan();
    }

    public override void AttackOpponentsChessMan(List<ChessMan> opponentChessMans)
    {
        base.AttackOpponentsChessMan(opponentChessMans);
    }
}
