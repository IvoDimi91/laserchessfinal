﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class NextLevelButtonClicked : MonoBehaviour {
	
	public string LevelToLoad;
	
	public void LoadLevel() {
        //Load level
        SceneManager.LoadScene(LevelToLoad);
    }
}
