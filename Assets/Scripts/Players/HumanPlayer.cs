﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HumanPlayer : Player {

    public Button endTurnButton;

    public HumanPlayer()
    {
        isHuman = true;
    }

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        
    }

    public override void Play()
    {
        base.Play();
        endTurnButton.gameObject.SetActive(true);
        endTurnButton.enabled = true;
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                if (hit.transform.GetComponent<ChessMan>() != null)
                {
                    ChessMan c = hit.transform.GetComponent<ChessMan>();
                    
                    if (c.playerCode == GameManager.GetInstance().players[GameManager.GetInstance().currentPlayerIndex].playerNumber)
                    {
                        GameManager.GetInstance().SelectChessMan(c);
                    }
                    else if (c.playerCode != GameManager.GetInstance().players[GameManager.GetInstance().currentPlayerIndex].playerNumber)
                    {
                        if (GameManager.GetInstance().selectedChessMan != null 
                            && GameManager.GetInstance().selectedChessMan.attacksPerTurn > 0 
                            && !GameManager.GetInstance().selectedChessMan.canAttackSimulteniously)
                        {
                            List<ChessMan> attackedChessMans = new List<ChessMan>();
                            attackedChessMans.Add(c);
                            GameManager.GetInstance().selectedChessMan.AttackOpponentsChessMan(attackedChessMans);
                        }
                    }
                }
            }
        }
        
        if (GameManager.GetInstance().selectedChessMan != null && GameManager.GetInstance().selectedChessMan.movesPerTurn > 0)
        {
            GameManager.GetInstance().selectedChessMan.MoveChessMan();
        }
    }

    public override void TurnOnGUI(ChessMan selectedChessMan)
    {
        if (selectedChessMan != null && selectedChessMan.movesPerTurn == 0 && selectedChessMan.attacksPerTurn == 0)
        {
            selectedChessMan.transform.GetComponent<Renderer>().material.color = GameManager.GetInstance().players[GameManager.GetInstance().currentPlayerIndex].chessManUsedColor;
        }

        int availableChessMans = playerChessMans.Count;
        if (playerChessMans != null && playerChessMans.Count > 0)
        {
            foreach (ChessMan c in playerChessMans)
            {
                if (c.movesPerTurn == 0 && c.attacksPerTurn == 0)
                {
                    availableChessMans--;
                }
            }
        }

        if(availableChessMans == 0)
        {
            endTurnButton.gameObject.transform.GetComponent<Image>().color = Color.green;
        } else
        {
            endTurnButton.gameObject.transform.GetComponent<Image>().color = Color.white;
        }
        base.TurnOnGUI(selectedChessMan);
    }

    public override void EndTurn()
    {
        base.EndTurn();
        GameManager.GetInstance().RemoveTileHighlights();
        if (GameManager.GetInstance().selectedChessMan != null)
        {
            GameManager.GetInstance().selectedChessMan.movesPerTurn = 1;
            GameManager.GetInstance().selectedChessMan.attacksPerTurn = 1;
        }
        GameManager.GetInstance().ResetAllChessMans();
        GameManager.GetInstance().NextTurn();
        endTurnButton.gameObject.SetActive(false);
        endTurnButton.enabled = false;
    }
}
