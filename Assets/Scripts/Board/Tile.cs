﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    [HideInInspector]
    public Vector2 gridPosition = Vector2.zero;

    // Handle different type of terrain
    public int movingCost = 1;
    public bool isImpassible = false;
    [HideInInspector]
    public List<Tile> neighbors;

    // Use this for initialization
    void Start()
    {
        GenerateNeighbors();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnMouseDown()
    {
        if(!this.isImpassible && GameManager.GetInstance().availableTilesForMovement.Contains(this) 
            && GameManager.GetInstance().selectedChessMan != null && GameManager.GetInstance().selectedChessMan.movesPerTurn > 0)
        {
            GameManager.GetInstance().FindAvailablePathForSelectedChessMan(this);
        }
    }

    private void GenerateNeighbors()
    {
        neighbors = new List<Tile>();
        //up
        if (gridPosition.y > 0)
        {
            Vector2 n = new Vector2(gridPosition.x, gridPosition.y - 1);
            neighbors.Add(GameManager.GetInstance().board[(int)Mathf.Round(n.x)][(int)Mathf.Round(n.y)]);
        }
        //down
        if (gridPosition.y < GameManager.GetInstance().board.Count - 1)
        {
            Vector2 n = new Vector2(gridPosition.x, gridPosition.y + 1);
            neighbors.Add(GameManager.GetInstance().board[(int)Mathf.Round(n.x)][(int)Mathf.Round(n.y)]);
        }
        //left
        if (gridPosition.x > 0)
        {
            Vector2 n = new Vector2(gridPosition.x - 1, gridPosition.y);
            neighbors.Add(GameManager.GetInstance().board[(int)Mathf.Round(n.x)][(int)Mathf.Round(n.y)]);
        }
        //right
        if (gridPosition.x < GameManager.GetInstance().board.Count - 1)
        {
            Vector2 n = new Vector2(gridPosition.x + 1, gridPosition.y);
            neighbors.Add(GameManager.GetInstance().board[(int)Mathf.Round(n.x)][(int)Mathf.Round(n.y)]);
        }
    }
}
