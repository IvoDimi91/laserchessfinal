﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilePathFinder : MonoBehaviour
{

    public TilePathFinder()
    {

    }

    public static List<Tile> FindPath(Tile originTile, Tile destTile)
    {
        List<Tile> visited = new List<Tile>();

        List<TilePath> unvisited = new List<TilePath>();

        TilePath originPath = new TilePath();
        originPath.AddTileToPath(originTile);

        unvisited.Add(originPath);

        while (unvisited.Count > 0)
        {
            TilePath currentTilePath = unvisited[0];
            unvisited.Remove(unvisited[0]);

            int listSize = currentTilePath.listOfTiles.Count;

            if (visited.Contains(currentTilePath.listOfTiles[listSize-1]))
            {
                continue;
            }

            if (currentTilePath.listOfTiles[listSize - 1] == destTile)
            {
                currentTilePath.listOfTiles.Remove(originTile);
                return currentTilePath.listOfTiles;
            }

            visited.Add(currentTilePath.listOfTiles[listSize - 1]);

            foreach (Tile t in currentTilePath.listOfTiles[listSize - 1].neighbors)
            {
                if (t.isImpassible)
                {
                    continue;
                }
                TilePath newTilePath = new TilePath(currentTilePath);
                newTilePath.AddTileToPath(t);
                unvisited.Add(newTilePath);
            }
        }

        return null;
    }
}
