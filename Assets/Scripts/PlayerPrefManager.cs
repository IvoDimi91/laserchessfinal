﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefManager {

    public static int GetScore()
    {
        if (PlayerPrefs.HasKey("Score"))
        {
            return PlayerPrefs.GetInt("Score");
        }
        else
        {
            return 0;
        }
    }

    public static void SetScore(int score)
    {
        PlayerPrefs.SetInt("Score", score);
    }

    public static int GetHighscore()
    {
        if (PlayerPrefs.HasKey("Highscore"))
        {
            return PlayerPrefs.GetInt("Highscore");
        }
        else
        {
            return 0;
        }
    }

    public static void SetHighscore(int highscore)
    {
        PlayerPrefs.SetInt("Highscore", highscore);
    }

    // reset stored player state and variables back to defaults
    public static void ResetPlayerState(int startPoints)
    {
        Debug.Log("Player State reset.");
        PlayerPrefs.SetInt("Score", startPoints);       
    }
}
